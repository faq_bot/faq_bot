from fastapi import APIRouter, BackgroundTasks
from src.models.requests import TextRequest, QuestionScore, QuestionsResponse
from src.connections.broker import celery_app
from src.services.utils import print_logger_info

router = APIRouter()


@router.post("/search/", response_model=QuestionsResponse)
async def search(text_request: TextRequest, background_tasks: BackgroundTasks) -> QuestionsResponse:
    """
    Get a list of questions similar to the given one

    Args:
        text_request (TextRequest): text request
        background_tasks (BackgroundTasks): some background tasks

    Returns:
        QuestionsResponse: check requests.py
    """

    async_result = celery_app.send_task("classification", args=[text_request.text])

    valid_question = async_result.get()
    background_tasks.add_task(
        print_logger_info,
        text_request.text,
        valid_question,
    )

    # Get similar questions from pinecone if the given one is worth looking for similar.
    if valid_question is True:

        async_result = celery_app.send_task(
            "similarity_search",
            args=[
                text_request.text,
                text_request.message_id,
                text_request.similar_amount,
                text_request.threshold
            ])
        similar_questions = async_result.get().get("similar_questions")
    else:
        similar_questions = []

    # FIXME No exception handling here,
    #  but None instead of valid_question may be a result of a task

    return QuestionsResponse(
        valid_question=valid_question,
        similar_questions=[QuestionScore(**question) for question in similar_questions]
    )
