from celery import Celery

import os

celery_app = Celery(
    'tasks',
    broker=f"pyamqp://guest:guest@faq_bot-rabbitmq-1:5672",
    backend=f"redis://redis"
)