from typing import Union, Dict, List
from loguru import logger

from src.services.similarity_search import SimilaritySearch
from src.services.classification import QuestionClassifier
from src.connections.broker import celery_app


@celery_app.task(name="classification")
def predict_question(text) -> Union[bool, None]:
    """
    Predict whether a question is suitable for further search of similar.

    Args:
        text (str): Question text
    """
    try:
        result = QuestionClassifier.predict(text)
        # {'Question text': True} (or False) format expected.
        # Take the very first value of this dictionary and that's it
        return next(iter(result.values()))
    except Exception as ex:
        logger.error([ex])
        return None


@celery_app.task(name="similarity_search")
def find_similar(text: str, message_id: float, similar_amount: int, threshold: float)\
        -> Union[Dict[str, List[Dict[str, Union[str, float, int]]]], None]:
    """
    Find similar questions with vector similarity search.

    Args:
        text (str): question to be analyzed.
        message_id (int): -- message id of the question.
        similar_amount (int) -- how many similar questions to return
        threshold (float) -- similarity threshold (between 0 and 1):

    Returns:
        {'Similar question text': score} dict.
    """
    # TODO upload good (or all?) questions with message_id back to pinecone
    try:
        result = SimilaritySearch.get_similar(
            text,
            similar_amount,
            threshold
        )
        return result
    except Exception as ex:
        logger.error([ex])
        return None
