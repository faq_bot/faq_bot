from pydantic import BaseModel
from typing import List


class TextRequest(BaseModel):
    """
    Model for text request (question to which similar ones to be found)

    Attributes:
        text (str): question to be analyzed.
        message_id (int): -- message id of the question.
        similar_amount (int): -- maximum amount of similar questions to return.
                                Note that it's not guaranteed.
        threshold (float): -- similarity threshold (between 0 and 1)

    If no message_id is found -- don't put to database your question
    """

    text: str
    message_id: int = 0
    similar_amount: int = 10
    threshold: float = 0.8

    class Config:
        """Model example."""

        schema_extra = {
            "example": {
                "text": "Помогите выбрать палатку?",
                "message_id": 135,
                "similar_amount": 10,
                "threshold": 0.8
            }
        }


class QuestionScore(BaseModel):
    """
    Represents a question messages with a similarity score. (Similarity to the given one)

    Attributes:
        question (str): A question from the database.
        score (float): The probability score of the question.
        message_id (int): message id of the question
    """
    question: str
    score: float
    message_id: int


class QuestionsResponse(BaseModel):
    """
    List of questions with given probabilities

    Attributes:
        valid_question (bool): Whether the question is valid (=="good" to find similar ones)
        similar_questions (List[QuestionScore]): A list of questions with their respective scores.
    """
    valid_question: bool = False
    similar_questions: List[QuestionScore]

    class Config:
        schema_extra = {
            "example": {
                "valid_question": True,
                "similar_questions": [
                    {"question": "Какую палатку выбрать?", "score": 0.29467877745628357, "message_id": 13},
                    {"question": "Что из палаток посоветуете?", "score": 0.25542123345632215, "message_id": 15},
                    {"question": "Как выбрать спальник?", "score": 0.18001219854345012, "message_id": 18}
                ]
            }
        }
