from typing import Dict

from loguru import logger
from clearml import Task, StorageManager
import torch.nn as nn
import transformers
from transformers import AutoTokenizer
import torch

# from dotenv import load_dotenv
# load_dotenv()

torch.set_num_threads(1)
# Set the thread number to 1.
# Might be needed for transformers pipelines)


class TransformerClassificationModel(nn.Module):
    """BERT + some classifier on the top."""

    def __init__(self, base_bert: str):
        """Init pretrained bert as well as custom classifier.

        Args:
            base_bert: pretrained model name
        """
        super(TransformerClassificationModel, self).__init__()

        # upload bert-like model
        self.backbone = transformers.BertModel.from_pretrained(base_bert)

        self.dense = nn.Sequential(nn.Linear(768, 768 * 2),
                                   nn.ReLU(),
                                   nn.Linear(768 * 2, 2))

    def forward(self, inputs, mask, token_type_ids):
        """Forward pass.

        Args:
            inputs:
            mask:
            token_type_ids:

        Returns:
            output
        """
        _, output = self.backbone(inputs, attention_mask=mask, token_type_ids=token_type_ids, return_dict=False)
        output = self.dense(output)

        return output


class Classifier:
    """
    BERT + some classifier on the top

    Methods:
        predict: predict the class of the question (valid/good,
    """
    def __init__(self):
        # Reach the model state dict.
        task = Task.get_task(task_name="rubert-base-unfrozen", project_name="Search")
        clearml_model = task.get_models()["output"][-1]  # use latest
        model_artifact = StorageManager.get_local_copy(clearml_model.get_local_copy(), "model.pt")

        # Initialize the model and tokenizer.
        model = TransformerClassificationModel("DeepPavlov/rubert-base-cased")
        tokenizer = AutoTokenizer.from_pretrained("DeepPavlov/rubert-base-cased")

        # Load the state dict into the model.
        model.load_state_dict(torch.load(model_artifact, map_location=torch.device('cpu')))
        model.eval()
        self.model = model
        self.tokenizer = tokenizer

    def __call__(self, *args, **kwargs):
        return self.predict(*args, **kwargs)

    def predict(self, text: str) -> Dict[str, bool]:
        """
        Predict a class of a question.
        Args:
            text: input question

        Returns:
            {"question": bool}, True for valid question, False for others
        """
        # Prepare for batch inference
        texts = [text]
        tokenized_text = self.tokenizer(
            texts,
            return_tensors='pt',
            max_length=200,
            padding='max_length',
            truncation=True
        )

        with torch.no_grad():
            ids = tokenized_text['input_ids'].squeeze(dim=1)
            mask = tokenized_text['attention_mask'].squeeze(dim=1)
            token_type_ids = tokenized_text['token_type_ids'].squeeze(dim=1)
            outputs = self.model(ids, mask, token_type_ids).argmax(dim=1)
            question_values = [True if x == 1 else False for x in outputs]
            answers = {question: x for (question, x) in zip(texts, question_values)}

        return answers


class QuestionClassifier:
    """
    Question classifier wrapper upon pretrained model

    Methods:
        predict: Predicts whether question worth looking for similar or not.
    """
    model = Classifier()

    @classmethod
    def predict(cls, text: str) -> Dict[str, bool]:
        """
        Predicts whether question worth looking for similar or not.

        Args:
            text:

        Returns:
            True/False for 'good' and 'bad' questions.

        """
        result = cls.model(text)
        logger.info(result)
        return result
