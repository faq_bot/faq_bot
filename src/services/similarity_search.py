from typing import Dict, List, Union

import os
from sentence_transformers import SentenceTransformer
from pinecone import Pinecone


class SimilaritySearch:
    """
    Encodes text to embedding and finds similar questions within vector DB

    Methods:
        get_similar: Gets similar questions.
    """
    # messages embeddings: "text" -> 312-dim numpy array
    model = SentenceTransformer('cointegrated/rubert-tiny2')
    pc = Pinecone(api_key=os.getenv("PINECONE_API_KEY"))
    index = pc.Index(os.getenv("PINECONE_INDEX"))

    @classmethod
    def encode(cls, text: str):
        embedding = list(cls.model.encode(text).astype(float))
        return embedding

    @classmethod
    def get_similar(cls, text, similar_amount, threshold) \
            -> Dict[str, List[Dict[str, Union[str, float, int]]]]:
        """
        Get similar questions from pinecone database
        Args:
            text: Question text
            similar_amount: How many similar questions to find. Note that the amount is not guaranteed.
            threshold: Similarity threshold from 0 (less similar) to 1 (most similar)

        Returns:
            Examples:
                {"similar_questions":
                    [
                        {
                            "question": "а с какой палаткой ходишь?",
                            "score": 0.853250444,
                            "message_id": 4902
                        }
                    ]
                }
        """
        embedding = cls.encode(text)
        result = cls.index.query(
            vector=embedding,
            top_k=similar_amount,
            include_values=False,
            include_metadata=True
        )
        # Filter out by threshold
        result['matches'] = [match for match in result['matches'] if match['score'] >= threshold]

        # Convert to json with our format
        similar_questions = {
            "similar_questions": []
        }

        for match in result['matches']:
            transformed_entry = {
                "question": match['metadata']['message'],
                "score": match['score'],
                "message_id": int(match['id'])
            }
            similar_questions["similar_questions"].append(transformed_entry)

        return similar_questions

