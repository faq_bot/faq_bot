import gradio as gr
import httpx
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from src.api.routes.router import router as api_router
from loguru import logger
import os
import dotenv

dotenv.load_dotenv()

def get_app() -> FastAPI:
    """
    FastAPI app initialization.
    """
    fastapi_app = FastAPI(
        title="Similar question service",
        version="0.1.0",
        debug=False,
        description="ML service to find questions similar to the given one",
    )
    fastapi_app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["GET", "POST"],
        allow_headers=["*"],
    )

    fastapi_app.include_router(api_router, prefix="/api")
    logger.info("FastAPI application has been initialized")
    return fastapi_app


app = get_app()


"""
FRONT PART
"""

fastapi_url = os.environ.get("FAST_API_URL")

async def submit_to_fastapi(text, message_id, similar_amount, threshold):
    try:
        async with httpx.AsyncClient() as client:

            payload = {"text": text,
                       "message_id": message_id,
                       "similar_amount": similar_amount, 
                       "threshold": threshold}
            
            response = await client.post(f"{fastapi_url}/api/search/", json=payload)
            response.raise_for_status()
            valid = response.json()["valid_question"]
            formatted_output = ""
            for _, q in enumerate(response.json()["similar_questions"]):
                formatted_output += f"<h3>Question {_+1}:</h3> <p>{q['question']}</p>"
                formatted_output += f"<b>Score:</b> {q['score']}<br>"
                formatted_output += f"<b>Message ID:</b> {q['message_id']}<br><br>"

            return valid, formatted_output
        
    except httpx.HTTPStatusError as e:
        logger.error(f"HTTP error occurred: {e.response.status_code}")
        return "HTTP error occurred."
    
    except Exception as e:
        logger.error(f"An error occurred: {str(e)}")
        return "An error occurred."
    
description = """

## This application fetches and displays similar questions from telegram chat history based on the input question. 

## Features
- **Fetch Similar Questions**: Get a list of questions similar to the one provided.
- **Display Scores and Message IDs**: See the relevance score and message ID for each question.

## Limitations
- **Valid question**: If your question will be classified as "not as good as it can be" you will receive the Valid question: False response.
- **High threshold**: If you don't see the similar question but your input is valid, try to lower the threshold.
"""

gradio_interface = gr.Interface(theme=gr.themes.Soft(),
    fn=submit_to_fastapi,
    inputs=[
        gr.Textbox(label="Input text"),
        gr.Textbox(label="Input message id"),
        gr.Textbox(label="How many questions to sample?"),
        gr.Textbox(label="Enter threshold")
        ],
    outputs=[
        gr.Textbox(label="Valid question"),
        gr.HTML()
        ],
    title="FAQ service bot",
    description=description,
    examples=[
        ["Подскажите рюкзак на лето?", 23, 3, 0.5],
        ["Как дела?", 432, 5, 0.3], 
        ],
    cache_examples="lazy"
)

app = gr.mount_gradio_app(app, gradio_interface, path="/gradio")
