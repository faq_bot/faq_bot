import telebot
import httpx
import asyncio
import os
from dotenv import load_dotenv

load_dotenv()

fastapi_url = "http://fastapi:7010" # use this url to interact between docker containers
    
async def submit_to_fastapi(text, message_id, similar_amount, threshold, chat_id_str):
    """
    Submit the provided text to the FastAPI endpoints for processing and return the results.

    Args:
        text (str): The message text to submit.
        message_id (int): The ID of the message.
        similar_amount (int): The number of similar questions to retrieve.
        threshold (float): The similarity threshold for filtering questions.
        chat_id_str (str): The chat ID string for generating message links.

    Returns:
        tuple: A tuple containing:
            - valid (bool): Whether the question is valid.
            - formatted_output (str): The formatted string of similar questions and their details.
            - number_of_samples (int): The number of similar questions returned.
    """
    async with httpx.AsyncClient() as client:
        payload = {
            "text": text,
            "message_id": message_id,
            "similar_amount": similar_amount,
            "threshold": threshold
        }
        
        response = await client.post(f"{fastapi_url}/api/search/", json=payload)
        response.raise_for_status()
        valid = response.json()["valid_question"]
        formatted_output = ""
        q_num = 0
        
        for q_num, q in enumerate(response.json()["similar_questions"]):
            formatted_output += f"Question {q_num + 1}: {q['question']}\n"
            formatted_output += f"Similarity Score: {q['score']:.3f}\n"
            formatted_output += f"Message link: https://t.me/c/{chat_id_str}/{q['message_id']}\n\n"
        number_of_samples = q_num + 1

        return valid, formatted_output, number_of_samples

BOT_TOKEN = os.environ.get("TG_BOT_TOKEN")
bot = telebot.TeleBot(token=BOT_TOKEN)

@bot.message_handler(func=lambda message: True)
def log_message(message):
    """
    Handle incoming messages, process them by submitting to FastAPI, and send the response back to the user.

    Args:
        message (telebot.types.Message): The incoming Telegram message.
    """
    chat_id_str = str(message.chat.id).replace("-100", "")
    message_text = message.text
    message_id = message.message_id
    similar_amount = 5  
    threshold = 0.5

    # Create a new event loop and set it as the current event loop
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    # Submit the message text to the FastAPI endpoint and get the results
    valid, formatted_output, number_of_samples = loop.run_until_complete(
        submit_to_fastapi(message_text, message_id, similar_amount, threshold, chat_id_str)
    )

    # Determine the reply text based on the validity and content of the response
    reply_text = formatted_output if valid else "Unacceptable question. Try again with another."
    if valid and len(formatted_output) != 0: 
        reply_text = f"Top {number_of_samples} similar questions:\n\n" + formatted_output
    elif not valid: 
        reply_text = "Unacceptable question. Try again with another."
    else: 
        reply_text = "No questions for sampling"

    # Send the reply back to the user
    bot.send_message(message.chat.id, reply_text)
    
# Start polling for new messages
bot.polling(none_stop=True)
